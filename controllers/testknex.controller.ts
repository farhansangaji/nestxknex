import { Body, Controller, Get, Param, Post, Query } from "@nestjs/common";
import { ICreateTestingDTO, IUpdateTestingDTO } from "interfaces/test.interface";
import { TestService } from "services/test.service";

@Controller('test')
export class TestController{
    constructor(private readonly testService : TestService){}

    @Post('create')
    async createTest(@Body() body:ICreateTestingDTO){
        try {
            const result = await this.testService.createTest(body)

            return result
        } catch (error) {
            return error
        }

    }

    @Post('update/:id')
    async updateTest(@Param() params : number, @Body() body : ICreateTestingDTO){
        const result = await this.testService.updateTest(body, params)

        return result
    }

    @Get(':id')
    async getTest(@Param() params : {id : number}){
        const result = await this.testService.getTest(params.id)

        return result
    }

    @Get('list')
    async getListTest(@Query() query : {angka : number}){
        const result = await this.testService.getList(query.angka)

        return result
    }
}