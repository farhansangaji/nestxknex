import { Controller, Get, Param } from '@nestjs/common';
import { Product } from 'interfaces/product.interface';
import { ProductService } from '../services/product.service';

@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get()
  async findAll(): Promise<Product[]> {
    return this.productService.findAll();
  }

  @Get(':id')
  async findOne(@Param() params): Promise<Product[]> {
    return this.productService.findOne(params.id);
  }
}
