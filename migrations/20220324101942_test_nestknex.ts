import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('testknexnest', (table) => {
    table.increments('id').primary();
    table.string('lmao');
    table.integer('angka');
    table.boolean('apakah');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('testknexnest');
}
