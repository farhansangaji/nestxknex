import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('relationTest', (table) => {
        table.increments('id').primary();
        table.integer('test_id');
        table.uuid('secretuid')
      });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('relationTest'); 
}

