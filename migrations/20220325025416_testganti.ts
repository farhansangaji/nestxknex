/* eslint-disable prettier/prettier */
import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('testknexnest', table => {
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('testknexnest', table => {
        table.dropColumn('created_at')
        table.dropColumn('updated_at')
    })
}

