interface baseEntity {
    id : number,
    created_at : Date
    updated_at : Date
}

export interface Testing extends baseEntity {
    lmao : string,
    angka : number,
    apakah : boolean,
    relation : RelationTest[]
}

export interface RelationTest extends baseEntity {
    test_id : number,
    secretuid : string
}

export interface ICreateTestingDTO {
    lmao : string,
    angka : number,
    apakah : boolean
}

export interface IUpdateTestingDTO extends ICreateTestingDTO{
    id : number
}