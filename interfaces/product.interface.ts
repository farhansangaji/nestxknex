export interface Product {
    id: number;
    brand_id: number;
    category_id: number;
    name?: string,
    sku: string,
    description?: string,
    weight: number,
    tagging?: string,
    status: number,
    created_by: number,
    deleted_by?: number
}
  