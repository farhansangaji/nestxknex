import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from 'modules/product.module';
import { KnexModule } from 'nest-knexjs';
import { TestModule } from 'modules/test.module';

@Module({
  imports: [
    ProductModule,
    TestModule,
    KnexModule.forRoot({
      config: {
        client: 'mysql',
        useNullAsDefault: true,
        connection: {
          host: 'localhost',
          port: 3306,
          user: 'root',
          password: 'Anaksql1!',
          database: 'what',
        },
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
