import { Knex } from 'knex';
import { Product } from 'interfaces/product.interface'
import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nest-knexjs';

@Injectable()
export class ProductService {
  constructor(@InjectModel() private readonly knex: Knex) {}

  async findAll(): Promise<Product[]> {
    const products: Product[] = await this.knex<Product>('products');
    return products;
  }

  async findOne(id: number): Promise<Product[]> {
    const products: Product[] = await this.knex<Product>('products').where('id', id);
    return products;
  }
}
