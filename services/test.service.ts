import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { randomUUID } from "crypto";
import { ICreateTestingDTO, IUpdateTestingDTO, Testing } from "interfaces/test.interface";
import { Knex } from "knex";
import { InjectModel } from "nest-knexjs";

@Injectable()
export class TestService {
    constructor(
        @InjectModel() private readonly knex: Knex,){}

    async createTest(payload : ICreateTestingDTO){
        try {
            const result = await this.knex.transaction( async function(trx){
                await trx.insert({
                    lmao : payload.lmao,
                    angka : payload.angka,
                    apakah : payload.apakah 
                }).into('testknexnest').then(async function(id){
                    await trx.insert({
                        secretuid : randomUUID(),
                        test_id : id
                    }).into('relationTest')
                }).then(trx.commit).catch(trx.rollback)
            })

            return result
        } catch (error) {
            throw new HttpException("Error Input", HttpStatus.BAD_REQUEST)
        }
    }

    async updateTest(payload : ICreateTestingDTO, id: number){
        const result = await this.knex.update({
            lmao : payload.lmao,
            angka : payload.angka,
            apakah : payload.apakah
        }).where(id).table('testknexnest')

        return "Result";
    }

    async getTest(id : number){
        const result = await this.knex.select('*').where('testknexnest.id', id)
                        .with('kolomkolom', this.knex.select('*').from("relationTest").where('relationTest.test_id', id)).from('testknexnest')
                        .leftJoin('kolomkolom','testknexnest.id' ,'kolomkolom.test_id').options({nestTables : true})
                        //get by id still not optimal. must use mapping with multiple queries or use 3rb party like objection.js

        // const dengan = await this.knex.with('kolomkolom', this.knex<Testing>('testknexnest').select('*').where('testknexnest.id', id)
        // ).select('*').from('kolomkolom')

        return result;
    }

    async getList(angka : number){
        let query = this.knex.select().table<Testing>('testknexnest')

        if(angka){
            query.andWhere({
                angka : angka
            })
        }

        const result = await query
        
        return result
    }

    async deleteTest(id : number){
        const result = await this.knex.select().table<Testing>('testknexnest')


    }

}